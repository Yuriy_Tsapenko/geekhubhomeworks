package com.mypackage;

public class Boat extends Vehicle {

public Boat(int gasTank){
    this.wheels = 0;
    this.gasTank = gasTank;
}
public int getWheels(){
    return this.wheels;
}

    @Override
    public void turn(){
        super.turn();

    }

    @Override
    public void breaking() {
        super.breaking();
    }

    @Override
    public void accelerate() {
        super.accelerate();
        this.speed+=this.enginePower*10;
    }
}
