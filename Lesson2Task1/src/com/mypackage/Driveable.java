package com.mypackage;

public interface Driveable {
    void turn();
    void accelerate();
    void breaking();
}
