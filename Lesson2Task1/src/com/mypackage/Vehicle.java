package com.mypackage;

public abstract class Vehicle implements Driveable {
    protected int speed = 0;
    protected int wheels=0;
    protected int gasTank=0;
    protected int enginePower=0;

    Vehicle(){

    }
    @Override
    public void turn(){
        if(this.gasTank>0) {
            this.gasTank -= this.enginePower * 3;
        } else {
            System.out.println("Fuel is empty");
        }
    }

    @Override
    public void accelerate() {
        this.enginePower += 20;
        this.speed = this.enginePower * 5;//число взятое с потолка
    }

    @Override
    public void breaking() {
        this.enginePower = 0;
    }

    public int getWheels() {
        return wheels;
    }

    public int getEnginePower() {
        return enginePower;
    }

    public int getSpeed() {
        return speed;
    }

    public int getGasTank() {
        return gasTank;
    }


    public void getDetails(){
        System.out.println("It's speed "+this.getSpeed());
        System.out.println("It's level of fuel is "+this.getGasTank());
        System.out.println("It's engine power is "+this.getEnginePower());
        System.out.println("It's number of wheels is "+this.getWheels());
        System.out.println("-------------------------------------------");
    }
}
