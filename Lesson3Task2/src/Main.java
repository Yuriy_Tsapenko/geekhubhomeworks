import com.mypackage.LinkedList;

public class Main {
public static LinkedList linkedList;

    public static void main(String[] args) {
        linkedList = new LinkedList();

        linkedList.add(1);
        linkedList.add('2');
        Integer number = new Integer(3);
        linkedList.add(number);
        linkedList.add("abcd");
        linkedList.add("1234");
        System.out.println("Linked list : "+linkedList);
        System.out.println("size(): "+linkedList.size());
        System.out.println("remove(2) :"+linkedList.remove(2));
        System.out.println("get(4): "+linkedList.get(4));
        linkedList.add("some data",3);
        System.out.println("add(\"some data\",3)");
        System.out.println("Printing of linked list again: "+linkedList);
        System.out.println("size(): "+linkedList.size());
    }

}
