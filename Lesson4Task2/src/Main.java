import com.TaskManager.Task;
import com.TaskManager.TaskManager;
import com.TaskManager.CurrentDay;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;

public class Main {
    private static TaskManager taskManager = new TaskManager();

    public static void main(String[] args) {
        taskManager.addTask(new Task("homework","do first task of homework"), new Date(117,10,2));
        taskManager.addTask(new Task("work","prepare a month summary"),new Date(117,10,25));
        taskManager.addTask(new Task("work","commit a secondary branch of project"),new Date(117,10,10));
        taskManager.printTasksList();


        System.out.println("\nTasks for today :");
        if(taskManager.getTasksForToday()!=null){
            for(Task task:taskManager.getTasksForToday()){
                System.out.println("["+task.getCategory()+"] "+task.getDescription());
            }
        }else{
            System.out.println("There is no tasks for today.");
        }

        System.out.println("\nCategories :");
        for(Object category:taskManager.getCategories()){
            System.out.println(category);
        }

        }

    }

