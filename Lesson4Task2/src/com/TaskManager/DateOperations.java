package com.TaskManager;

import java.util.Date;

public class DateOperations {
    public static boolean equals(Date firstDate, Date secondDate){
        boolean equals = false;
        if(firstDate.getDate() == secondDate.getDate()&&
                firstDate.getMonth() == secondDate.getMonth()&&
                firstDate.getYear() == secondDate.getYear())
            equals =true;
        return equals;
    }
}
