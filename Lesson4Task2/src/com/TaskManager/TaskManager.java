package com.TaskManager;

import java.util.*;

public class TaskManager implements TaskInterface {
    private List<Task> taskList= new ArrayList<>();
    private Map<Date, List<Task>> taskMap = new HashMap<>();

    @Override
    public void addTask(Task task, Date date) {
        if(taskMap.containsKey(date)){
            List current;
            current = taskMap.get(date);
            current.add(task);
            taskMap.put(date,current);
        }
        else{
            List<Task> current = new ArrayList<>();
            current.add(task);
            taskMap.put(date,current);
        }
    }

    @Override
    public void removeTask(Date date) {
        taskMap.remove(date);
    }

    @Override
    public Collection getCategories() {
        Collection categories = new HashSet();
        for(Map.Entry<Date,List<Task>> entry: taskMap.entrySet()){
            for(Task task:entry.getValue()){
                categories.add(task.getCategory());
            }
        }
        return categories;
    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        List tasks = new ArrayList();
        for(Map.Entry entry: taskMap.entrySet() ){
            for(Task task: entry.getValue()){

            }
        }
        return tasks;
    }

    @Override
    public List<Task> getTasksForToday() {
        Date today = new CurrentDay();

        if (taskMap.containsKey(today)){
           List<Task> tasks = taskMap.get(today);
            return tasks;
        }else return null;

    }


    private int i;
    public void printTasksList(){
        System.out.println("Task list");
        for(Map.Entry<Date, List<Task>> entry:taskMap.entrySet()){

            List<Task> tasks = entry.getValue();
            i = 0;
            for(Task task:tasks){
                if(i==0){
                    Date date = entry.getKey();
                    System.out.print(String.format("%2d",date.getDate())+"/"+String.format("%2d",date.getMonth()+1)+
                            "/"+(date.getYear()+1900)+"  ");
                    i++;
                }
                else{
                    System.out.print(String.format("%12s"," "));
                }
                    System.out.print(" ["+task.getCategory()+"] ");
                    System.out.println(task.getDescription());
            }
        }
    }
}
