package com.TaskManager;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public interface TaskInterface {
    public void addTask(Task task, Date date);
    public void removeTask(Date date);
    public Collection getCategories();
    public List<Task> getTasksByCategory(String category);
    public List<Task> getTasksForToday();
}
