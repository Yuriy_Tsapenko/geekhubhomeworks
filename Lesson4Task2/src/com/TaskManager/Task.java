package com.TaskManager;

import java.util.Date;

public class Task {
    private String category;
    private String description;
    private Date date;

    public Task(String category,String description){
        this.category = category;
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public String getDescription() {
        return description;
    }

}
