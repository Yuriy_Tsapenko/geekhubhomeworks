import com.mypackage.*;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        RandomString rndString = new RandomString();

        String string = new String();
        System.out.print("Please, input number of iterations ...");
        Scanner in = new Scanner(System.in);
        int iters = in.nextInt();
        long start,finish,timeString, timeStringBuffer, timeStringBuilder;
        start = System.currentTimeMillis();
        for(int i=0;i<iters;i++){
            string = string.concat(rndString.getRandomString(8));
        }
        finish = System.currentTimeMillis();
        timeString = finish-start;


        StringBuffer stringBuffer = new StringBuffer();
        start = System.currentTimeMillis();
        for(int i=0;i<iters;i++){
            stringBuffer.append(rndString.getRandomString(8));
        }
        finish = System.currentTimeMillis();

        timeStringBuffer = finish-start;

        StringBuilder stringBuilder = new StringBuilder();
        start = System.currentTimeMillis();
        for(int i=0;i<iters;i++){
            stringBuffer.append(rndString.getRandomString(8));
        }
        finish = System.currentTimeMillis();
        timeStringBuilder = finish - start;

        ConsoleDiagram cd = new ConsoleDiagram();
        System.out.println("Comparing of string concatenation time");
        cd.drawDiagram(timeString,timeStringBuffer,timeStringBuilder);
    }
}
