package com.mypackage;
import java.util.Random;
public class RandomString {
    private String letters = "abcdefghijklmnopqrstuvwxyz";
    private String numbers = "123456789";
    private String symbols = letters + letters.toUpperCase()+numbers;

    public String getRandomString(int length){
        String result = "";
        Random rnd = new Random();
        for(int i=0;i<length;i++){
            result = result.concat(""+symbols.charAt(rnd.nextInt(symbols.length())));
        }
        return result;
    }
}
