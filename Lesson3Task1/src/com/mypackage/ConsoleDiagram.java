package com.mypackage;

public class ConsoleDiagram {
    public static long round(long arg){
        long result;
        if(arg%5 >2) result = arg + (5-(arg%5));
        else result = arg - (arg%5);
        return result;
    }
    public void drawDiagram(long... values){
        long min=values[0], max=values[0];
        for(int i=0;i<values.length;i++){
            if(min>values[i]) min=values[i];
            if(max<values[i]) max=values[i];
        }
        long height=(round(max)-round(min))/5+1;

        for(int i=0;i<height+1;i++){
            if(i==0) System.out.print("↑");
            else {
                System.out.print("|");
                for(int j=0;j<values.length;j++){
                    if(i==(height-round(values[j]-min)/5)) System.out.print(String.format("%5d",values[j])+"|    ");
                    else if(i>(height-round(values[j]-min)/5)) System.out.print("     |    ");
                    else System.out.print("          ");

                }
            }
            System.out.println();
        }
        String xAxis = "+";
        for(int i=0;i<(values.length*10);i++){
            xAxis = xAxis.concat("-");
        }
        xAxis+="►";
        System.out.println(xAxis);
        System.out.println("String   StringBuilder   StringBuffer");
    }
}
