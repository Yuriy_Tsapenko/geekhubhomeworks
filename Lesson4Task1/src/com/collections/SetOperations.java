package com.collections;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetOperations implements SetInterface {
    @Override
    public boolean equals(Set a, Set b) {
        boolean equals = false;
        if(a.size()==b.size()){
            for(Object aItem:a){
            equals=false;
            for(Object bItem:b){
                if(aItem.equals(bItem)){
                    equals=true;
                    continue;
                }
            }
            if(equals==false) break;
            }
        }
        return equals;
    }

    @Override
    public Set subtract(Set a, Set b) {
        Set result = new HashSet();
        boolean contains = false;
        for(Object setAItem:a){
            contains = false;
            for(Object setBItem:b){
                if(setAItem.equals(setBItem)){
                    contains = true;
                }
            }
            if(contains==false) result.add(setAItem);
        }
        return result;
    }

    @Override
    public Set union(Set a, Set b) {
        Set result = new HashSet();
        for(Object item:a){
            result.add(item);
        }
        for(Object item:b){
            result.add(item);
        }
        return result;
    }

    @Override
    public Set itersect(Set a, Set b) {
        Set result = new HashSet();
        for(Object setAItem:a){
            for(Object setBItem:b){
                if(setAItem.equals(setBItem)) result.add(setAItem);
            }
        }
        return result;
    }

    @Override
    public Set symmetricSubtract(Set a, Set b) {
        Set result = new HashSet();
        for(Object setAItem:a){
            for(Object setBItem:b){
                if(!setAItem.equals(setBItem)){
                    result.add(setAItem);
                    result.add(setBItem);
                }
            }
        }
        return result;
    }

    public String toString(Set set){
        String result="";
        for(Object item:set){
            result+="("+item+")";
        }
        return result;
    }
}
