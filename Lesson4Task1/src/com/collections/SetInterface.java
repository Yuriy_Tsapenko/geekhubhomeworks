package com.collections;

import java.util.Set;

public interface SetInterface {
    public boolean equals(Set a, Set b);
    public Set union(Set a, Set b);
    public Set subtract(Set a, Set b);
    public Set itersect(Set a, Set b);
    public Set symmetricSubtract(Set a, Set b);
}
