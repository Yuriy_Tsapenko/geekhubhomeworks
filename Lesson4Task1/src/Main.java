import com.collections.SetOperations;

import java.util.HashSet;
import java.util.Set;

public class Main {


    public static void main(String[] args) {
        SetOperations setA = new SetOperations();
        Set set1 = new HashSet();
        Set set2 = new HashSet();

        set1.add("one");
        set1.add("two");
        set1.add("three");

        System.out.println("Set1 : "+set1.toString());
        set2.add("three");
        set2.add("four");
        set2.add("five");
        System.out.println("Set2 : "+set2.toString());
        System.out.println();
        Set resultSet = new HashSet();
        resultSet = setA.union(set1,set2);
        for(Object item:resultSet){
            System.out.println(item);
        }
    }
}
